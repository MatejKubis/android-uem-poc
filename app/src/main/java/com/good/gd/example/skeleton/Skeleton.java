/*
 *  This file contains sample code that is licensed according to the BlackBerry Dynamics SDK terms and conditions.
 *  (c) 2017 BlackBerry Limited. All rights reserved.
 */

package com.good.gd.example.skeleton;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;

import com.good.gd.GDAndroid;
import com.good.gd.GDStateListener;

import java.util.Map;
import java.util.Map.Entry;


/* Skeleton - the entry point activity which will start authorization with Good Dynamics
 * and once done launch the application UI.
 */
public class Skeleton extends Activity implements GDStateListener {

    private static final String TAG = Skeleton.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.skeleton_layout);

        findViewById(R.id.b_config).setOnClickListener(new OnClickListener() {
            @Override public void onClick(View v) {
                logConfig(GDAndroid.getInstance().getApplicationConfig());
            }
        });

        GDAndroid.getInstance().activityInit(this);

    }

    /*
     * Activity specific implementation of GDStateListener.
     *
     * If a singleton event Listener is set by the application (as it is in this case) then setting
     * Activity specific implementations of GDStateListener is optional
     */
    @Override
    public void onAuthorized() {
        //If Activity specific GDStateListener is set then its onAuthorized( ) method is called when
        //the activity is started if the App is already authorized
        Log.i(TAG, "onAuthorized()");
    }

    @Override
    public void onLocked() {
        Log.i(TAG, "onLocked()");
    }

    @Override
    public void onWiped() {
        Log.i(TAG, "onWiped()");
    }

    @Override
    public void onUpdateConfig(Map<String, Object> settings) {
        Log.i(TAG, "onUpdateConfig()");
        logConfig(settings);
    }

    @Override
    public void onUpdatePolicy(Map<String, Object> policyValues) {
        Log.i(TAG, "onUpdatePolicy()");
    }

    @Override
    public void onUpdateServices() {
        Log.i(TAG, "onUpdateServices()");
    }

    @Override
    public void onUpdateEntitlements() {
        Log.i(TAG, "onUpdateEntitlements()");
    }


    private void logConfig(Map<String, Object> applicationConfig) {
        for (Entry<String, Object> entry : applicationConfig.entrySet()) {
            Log.i(TAG, "Config item: " + entry.getKey() + ":" + entry.getValue());
        }
    }
}
